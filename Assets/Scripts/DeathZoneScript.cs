﻿using UnityEngine;
using System.Collections;

public class DeathZoneScript : MonoBehaviour {

	static PaddleScript paddleScript;
	public int pointValue = 200;
	// Use this for initialization
	void Start () {
		paddleScript = GameObject.Find ("paddle").GetComponent<PaddleScript> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter( Collider other) 
	{
//		Debug.Log (other.gameObject+" DeathZone Trigger");
		if(other.gameObject.name == "ball"){
			Debug.Log ("ball!");
			BallScript ballScript = other.GetComponent<BallScript> ();
			if(ballScript)
			{
				Debug.Log (other.gameObject.tag+" FindWithTagpowerUp!");
				paddleScript.AddPoints(pointValue, 2);
				/*if(other.gameObject.color == "red")
					paddleScript.AddPoints(pointValue, 2);
				else if(other.gameObject.color = "blue")
					paddleScript.AddPoints(pointValue, 1);*/
					
				ballScript.Die();
			}
		}
		else if(other.gameObject.name == "powerUp"){
			Debug.Log ("powerUp!");
			PowerUpScript powerUpScript = other.GetComponent<PowerUpScript>();
			if(powerUpScript)
			{
				powerUpScript.Die();
			}
		}
		else if(other.gameObject.name == "brick_fragment"){
//			Debug.Log ("fragment!");
			Destroy (other.gameObject);
		}
	}
}
